FROM ruby:2.7
COPY entrypoint.sh /entrypoint.sh 
RUN chmod +x /entrypoint.sh 
ENTRYPOINT ["/bin/sh", "/entrypoint.sh"]
